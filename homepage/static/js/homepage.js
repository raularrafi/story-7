$(document).ready(function(){
		$(".content").hide();
		$("#button-activity").click(function(){
			console.log("test");
  			$("#content-activity").slideToggle();
  			$("#content-experience").hide("slow");
  			$("#content-achievement").hide("slow");
		});
		$("#button-experience").click(function(){
			console.log("test");
  			$("#content-experience").slideToggle();
  			$("#content-activity").hide("slow");
  			$("#content-achievement").hide("slow");
		});
		$("#button-achievement").click(function(){
			console.log("test");
  			$("#content-achievement").slideToggle();
  			$("#content-experience").hide("slow");
  			$("#content-activity").hide("slow");
		});
		$("#theme-checkbox").change(function(){
			if ($("body").hasClass("dark-mode")) {
				$("body").removeClass("dark-mode");
			} else {
				$("body").addClass("dark-mode");
			}
		});
});
