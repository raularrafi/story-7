from django.test import TestCase
from django.test import Client
from django.urls import resolve
from homepage.views import homepage
from django.http import HttpRequest

class UnitTest(TestCase):

	def test_homepage_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_notexist_url_is_notexist(self):
		response = Client().get('/profile/')
		self.assertEqual(response.status_code, 404)

	def test_homepage_using_homepage_function(self):
		response = resolve('/')
		self.assertEqual(response.func, homepage)

	def test_homepage_using_homepage_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')

	def test_homepage_title_is_right(self):
		request = HttpRequest()
		response = homepage(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<title>RAD</title>', html_response)

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time

class FunctionalTest(TestCase):
	def setUp(self):
		chrome_options = webdriver.ChromeOptions()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium = webdriver.Chrome(chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(FunctionalTest, self).tearDown()

	def test_accordion_displayed(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')

		self.assertIn("Activity", selenium.page_source)
		self.assertIn("Experience", selenium.page_source)
		self.assertIn("Achievement", selenium.page_source)

		accordion = selenium.find_element_by_id('button-activity')

		accordion.click()
		self.assertIn("CS Student at UI", selenium.page_source)

	def test_normal_mode_using_white_background(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		backgroundObj = selenium.find_element_by_tag_name('body')
		self.assertEqual("rgba(255, 255, 255, 1)", backgroundObj.value_of_css_property("background-color"))


